import React from 'react'

import {
  Container
} from './styled'

const Main = ({ children }) => (
  <Container className='Main'>
    {children}
  </Container>
)

export default Main
