import React, { Component } from 'react'

import Container from './styled';

export default class Sidebar extends Component {
  render() {
    const { children } = this.props;

    return (
      <Container className='Sidebar'>
        {children}        
      </Container>
    )
  }
}
