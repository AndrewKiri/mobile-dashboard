import styled from 'styled-components'

import CommonContainer from 'common/Container'
import { elevation } from './elevation';

const Container = styled(CommonContainer)`
  top: 0;
  right: 0;
  bottom: 0;
  position: fixed;
  z-index: 3;
  ${elevation(3, false)}
`

const Shadow = styled(CommonContainer)`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 2;
  pointer-events: none;
  background-color: rgba(0,0,0,0.8);
`

const HeaderWrapper = styled(CommonContainer)`
  position: relative;
`

const Content = styled(CommonContainer)`
  position: relative;
  overflow-y: auto;
`

export {
  Container,
  Shadow,
  HeaderWrapper,
  Content,
}
