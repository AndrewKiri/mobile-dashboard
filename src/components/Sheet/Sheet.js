import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import TWEEN from '@tweenjs/tween.js'

import {
  Container,
  Shadow,
  HeaderWrapper,
  Content,
} from './styled'

const classNames = require('classnames');

export default class Sheet extends Component {
  constructor(props) {
    super(props)

    const { shouldOpen } = props

    this.state = {
      open: shouldOpen,
      sheetOffset: null,
      dragStartX: 0,
      direction: '',
      width: 0,
      thresholdPercent: 0.2,
      swipeThreshold: 0,
      avgVelocity: 0,
      percentageDragged: !shouldOpen ? 1 : 0,
    }
  }

  static propTypes = {
    shouldOpen: PropTypes.bool,
    isScrollingSheet: PropTypes.bool,
  }

  static defaultProps = {
    shouldOpen: false,
  }

  componentDidMount() {
    const { thresholdPercent } = this.state;
    const { shouldOpen } = this.props;
    const width = window.innerWidth;
    const swipeThreshold = Math.ceil(width * thresholdPercent);

    this.setState({
      width,
      sheetOffset: !shouldOpen ? width : 0,
      swipeThreshold
    })
  }

  componentDidUpdate(prevProps) {
    const { shouldOpen } = this.props;
    const prevShouldOpen = prevProps.shouldOpen;
    const { open } = this.state;

    if (prevShouldOpen !== shouldOpen) {
      if (!open) {
        this.openSheet()
      }
      
      else {
        this.closeSheet()
      }
    }
  }

  resetSheet = () => {
    const { sheetOffset, width } = this.state;
    
    function animate() {
      if (TWEEN.update()) {
        requestAnimationFrame(animate);
      }
    }

    new TWEEN.Tween({ val: sheetOffset })
      .to({ val: 0 }, 200)
      .easing(TWEEN.Easing.Quadratic.Out)
      .start()
      .onUpdate((o) => this.setState({
        sheetOffset: o.val,
        percentageDragged: o.val / width
      }));

    animate();
  }

  closeSheet = () => {
    const {
      sheetOffset,
      width
    } = this.state;

    function animate() {
      if (TWEEN.update()) {
        requestAnimationFrame(animate);
      }
    }

    new TWEEN.Tween({ val: sheetOffset })
      .to({ val: width }, 200)
      .easing(TWEEN.Easing.Quadratic.Out)
      .start()
      .onUpdate((o) => this.setState({
        sheetOffset: o.val,
        percentageDragged: o.val / width
      }))
      .onComplete(() => this.setState({ open: false }));

    animate();
  }

  openSheet = () => {
    this.setState({
      open: true,
    }, () => {
      this.resetSheet();
    })
  }

  handleTouchStart = (event) => {
    const { clientX } = event.targetTouches[0];

    this.setState({
      dragStartX: clientX,
    });
  }

  handleTouchMove = (event) => {
    const {
      sheetOffset,
      dragStartX,
      width,
      swipeThreshold,
      avgVelocity,
    } = this.state;
    if (dragStartX > swipeThreshold) return;
    const newClientX = event.targetTouches[0].clientX;

    const calc = -(dragStartX - newClientX);
    const newOffset = calc > 0 ? calc : 0;
    const direction = sheetOffset > newOffset ? 'left' : 'right';
    const velocity = Math.abs(sheetOffset - newOffset);
    const percentage = newOffset / width;
    const percentageDragged = percentage < 1
      ? percentage
      : 1;

    this.setState({
      direction,
      sheetOffset: newOffset,
      percentageDragged,
      avgVelocity: (avgVelocity + velocity) / 2,
    })
  }

  handleTouchEnd = () => {
    const {
      percentageDragged,
      direction,
      thresholdPercent,
      avgVelocity,
    } = this.state;
    const shouldClose = direction === 'right'
      && (percentageDragged > thresholdPercent || avgVelocity > 1);

    if (shouldClose ) {
      this.closeSheet();
    } else {
      this.resetSheet();
    }

    this.setState({
      dragStartX: 0,
      direction: ''
    })
  }

  render() {
    const {
      shouldOpen,
      renderHeader,
      renderContent
    } = this.props;
    const {
      percentageDragged,
      open,
      sheetOffset,
      width,
    } = this.state;

    const shadowOpacity = .8 - percentageDragged;
    const translateX = open
      ? `translateX(${sheetOffset}px)`
      : `translateX(${width}px)`

    return (
      <Fragment>
        <Shadow
          className='Shadow' 
          style={{
            opacity: shadowOpacity,
            display: shadowOpacity < 0 ? 'none' : 'initial'
          }}
        />
        <Container
          className={classNames('Container', { shouldOpen })}
          shouldOpen={shouldOpen}
          ref={element => this.myElement = element}
          onTouchEnd={this.handleTouchEnd}
          onTouchMove={this.handleTouchMove}
          onTouchStart={this.handleTouchStart}
          style={{
            transform: translateX
          }}
        >
          <HeaderWrapper className='HeaderWrapper'>
            {renderHeader}
          </HeaderWrapper>
          <Content className='Content'>
            {renderContent}
          </Content>
        </Container>
      </Fragment>
    )
  }
}
