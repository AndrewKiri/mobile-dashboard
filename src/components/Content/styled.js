import styled from 'styled-components'

import CommonContainer from 'common/Container'

const Container = styled(CommonContainer)`
  display: flex;
  flex-direction: row;
  background-color: #f7f7f7;
  height: 100%;
  min-height: 100vh;
  width: 100%;
`

export {
  Container
}
