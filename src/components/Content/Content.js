import React from 'react'

import {
  Container
} from './styled'

const Content = ({ children }) => (
  <Container className='Content'>
    {children}
  </Container>
)

export default Content
