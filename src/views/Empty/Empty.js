import React, { Component } from 'react'

import {
  Container
} from './styled'

export default class Empty extends Component {
  render () {
    return (
      <Container  className='Empty'>
        This is Empty
      </Container>
    )
  }
}
