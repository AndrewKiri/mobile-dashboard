import styled from 'styled-components'

import CommonContainer from 'common/Container'

const Container = styled(CommonContainer)``

export {
  Container
}
