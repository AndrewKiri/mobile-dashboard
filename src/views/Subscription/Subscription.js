import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import {
  Container,
  // Button,
  // Content
} from './styled'

// import Sheet from 'components/Sheet'

class Subscription extends Component {
  state = {
    shouldOpen: false
  }

  toggleShowSheet = () => this.setState({
    shouldOpen: !this.state.shouldOpen
  })

  render() {
    // const { shouldOpen } = this.state;
    const { match } = this.props;
    const { params } = match;

    return (
      <Container className='Subscription'>
        {`showing data for product with ID ${params.id}`}
      </Container>
    );
  }
}

// <Button onClick={this.toggleShowSheet}>
//   click to open overlay
// </Button>

// <Sheet
// shouldOpen={shouldOpen}
// renderContent={(
//   <Content>
//   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Malesuada fames ac turpis egestas. Dictumst vestibulum rhoncus est pellentesque elit. Arcu non sodales neque sodales ut etiam sit amet nisl. Ut lectus arcu bibendum at. Diam phasellus vestibulum lorem sed risus ultricies tristique nulla. Aliquam id diam maecenas ultricies mi eget mauris. Integer feugiat scelerisque varius morbi. Sagittis aliquam malesuada bibendum arcu vitae elementum. Quis risus sed vulputate odio ut. Sed lectus vestibulum mattis ullamcorper velit. Mauris vitae ultricies leo integer. Nulla facilisi etiam dignissim diam quis enim. At imperdiet dui accumsan sit amet nulla facilisi morbi tempus. Consequat semper viverra nam libero justo. Egestas maecenas pharetra convallis posuere morbi. Sit amet aliquam id diam maecenas ultricies mi. Lacus viverra vitae congue eu. Morbi leo urna molestie at elementum eu facilisis.

//   Aliquet eget sit amet tellus cras adipiscing enim eu turpis. Et pharetra pharetra massa massa ultricies mi quis hendrerit dolor. Dictum at tempor commodo ullamcorper a. Tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse. Aliquet lectus proin nibh nisl condimentum id venenatis. Purus faucibus ornare suspendisse sed nisi lacus. Neque ornare aenean euismod elementum nisi quis eleifend quam. A diam sollicitudin tempor id eu nisl nunc mi. Tellus elementum sagittis vitae et leo duis ut diam quam. Diam maecenas ultricies mi eget. Et malesuada fames ac turpis egestas sed. At tempor commodo ullamcorper a lacus vestibulum.
  
//   Amet mattis vulputate enim nulla aliquet porttitor. Commodo nulla facilisi nullam vehicula ipsum. Non arcu risus quis varius quam quisque. Quam nulla porttitor massa id neque aliquam. Mauris a diam maecenas sed enim ut sem. Maecenas ultricies mi eget mauris pharetra. Iaculis at erat pellentesque adipiscing commodo elit. Pretium aenean pharetra magna ac. Imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper. Fermentum leo vel orci porta non pulvinar.
  
//   Dictum sit amet justo donec. Leo vel fringilla est ullamcorper. Feugiat pretium nibh ipsum consequat nisl vel pretium lectus. Sed vulputate odio ut enim blandit volutpat. Gravida cum sociis natoque penatibus et magnis dis parturient montes. Velit sed ullamcorper morbi tincidunt ornare massa eget. Sed tempus urna et pharetra pharetra. Libero enim sed faucibus turpis in eu. Facilisi morbi tempus iaculis urna id volutpat. In nibh mauris cursus mattis. Sed viverra tellus in hac.
  
//   Dictum sit amet justo donec enim diam vulputate ut pharetra. Sit amet massa vitae tortor condimentum lacinia quis vel. Fusce ut placerat orci nulla pellentesque dignissim enim sit amet. Amet volutpat consequat mauris nunc congue. Id diam vel quam elementum pulvinar etiam non quam. Orci nulla pellentesque dignissim enim sit amet. In nulla posuere sollicitudin aliquam ultrices sagittis orci. Odio morbi quis commodo odio aenean. Lectus quam id leo in vitae turpis massa. Sit amet purus gravida quis blandit turpis cursus in. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. In tellus integer feugiat scelerisque varius morbi enim.
  
//   Amet purus gravida quis blandit. Ornare lectus sit amet est placerat in egestas. Tortor aliquam nulla facilisi cras fermentum odio eu feugiat. Ipsum nunc aliquet bibendum enim facilisis gravida neque convallis a. Accumsan sit amet nulla facilisi morbi tempus iaculis. Nisi porta lorem mollis aliquam ut porttitor leo. Pharetra massa massa ultricies mi quis hendrerit dolor magna. Vitae tempus quam pellentesque nec nam aliquam. Lacus laoreet non curabitur gravida arcu ac tortor. Neque laoreet suspendisse interdum consectetur libero id faucibus nisl. Ultricies mi quis hendrerit dolor magna eget est. At imperdiet dui accumsan sit amet nulla facilisi. Arcu cursus vitae congue mauris rhoncus aenean vel. Volutpat ac tincidunt vitae semper quis lectus nulla at. Viverra tellus in hac habitasse platea dictumst vestibulum. Molestie a iaculis at erat pellentesque adipiscing. Dolor sit amet consectetur adipiscing elit ut.
  
//   Consectetur lorem donec massa sapien faucibus et molestie ac feugiat. Nunc lobortis mattis aliquam faucibus purus in massa tempor. Lorem dolor sed viverra ipsum. Dictum fusce ut placerat orci nulla pellentesque dignissim enim sit. Placerat orci nulla pellentesque dignissim enim. Senectus et netus et malesuada fames ac turpis. Enim ut tellus elementum sagittis vitae et. Eu consequat ac felis donec et odio. Elit ut aliquam purus sit amet luctus venenatis lectus. Sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Massa sed elementum tempus egestas sed sed risus pretium quam. Sed turpis tincidunt id aliquet risus feugiat in. Pellentesque habitant morbi tristique senectus et netus et malesuada. Metus aliquam eleifend mi in. At augue eget arcu dictum varius duis at consectetur lorem. Vitae justo eget magna fermentum iaculis eu non diam phasellus. Et malesuada fames ac turpis egestas sed. Purus in mollis nunc sed id semper.
  
//   Mauris nunc congue nisi vitae suscipit. Varius duis at consectetur lorem. Eget magna fermentum iaculis eu non diam phasellus vestibulum lorem. Aliquet sagittis id consectetur purus ut faucibus pulvinar elementum. Volutpat odio facilisis mauris sit amet massa vitae tortor condimentum. Consequat nisl vel pretium lectus quam id leo in vitae. Maecenas sed enim ut sem viverra aliquet eget sit. Gravida quis blandit turpis cursus in hac habitasse platea dictumst. Tristique senectus et netus et malesuada fames ac. Arcu risus quis varius quam quisque id. Velit egestas dui id ornare arcu odio ut sem. Semper quis lectus nulla at volutpat diam ut.
  
//   Ullamcorper malesuada proin libero nunc consequat. Eu non diam phasellus vestibulum lorem sed. Enim nulla aliquet porttitor lacus luctus accumsan tortor posuere ac. Quam id leo in vitae. Ultrices neque ornare aenean euismod elementum nisi quis. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Turpis in eu mi bibendum neque egestas. Mi sit amet mauris commodo quis. Adipiscing commodo elit at imperdiet dui accumsan. Tempor orci eu lobortis elementum nibh tellus molestie nunc. Erat nam at lectus urna duis convallis convallis tellus id. Ut tristique et egestas quis ipsum suspendisse ultrices gravida dictum. Velit aliquet sagittis id consectetur purus ut.
  
//   Enim facilisis gravida neque convallis a cras semper auctor neque. Feugiat in ante metus dictum at tempor commodo ullamcorper a. Nibh venenatis cras sed felis. Eget aliquet nibh praesent tristique magna sit amet purus. Feugiat in fermentum posuere urna nec tincidunt praesent. Ultricies mi quis hendrerit dolor magna eget est lorem ipsum. In hendrerit gravida rutrum quisque non tellus orci. Rhoncus est pellentesque elit ullamcorper dignissim. Interdum velit euismod in pellentesque massa placerat duis. Iaculis nunc sed augue lacus viverra vitae congue eu consequat.
  
//   Eleifend quam adipiscing vitae proin sagittis nisl rhoncus mattis. Mi ipsum faucibus vitae aliquet nec ullamcorper. Vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. Pharetra diam sit amet nisl. Leo vel fringilla est ullamcorper eget nulla facilisi etiam dignissim. Faucibus purus in massa tempor nec feugiat nisl. Vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras. Arcu non sodales neque sodales ut. Habitant morbi tristique senectus et netus. Semper quis lectus nulla at volutpat diam ut. Nibh tellus molestie nunc non blandit massa enim.
  
//   Id cursus metus aliquam eleifend mi. Ultricies integer quis auctor elit sed vulputate mi sit amet. Ligula ullamcorper malesuada proin libero nunc consequat interdum. Aliquam ultrices sagittis orci a scelerisque purus semper eget. Ac ut consequat semper viverra nam libero. At in tellus integer feugiat scelerisque varius morbi. Volutpat lacus laoreet non curabitur gravida arcu. Enim sit amet venenatis urna cursus eget nunc scelerisque viverra. Venenatis urna cursus eget nunc scelerisque viverra mauris in. Metus dictum at tempor commodo ullamcorper a. Leo urna molestie at elementum eu facilisis. Orci ac auctor augue mauris. Mi proin sed libero enim sed faucibus turpis in eu. Aliquam faucibus purus in massa tempor nec. Condimentum mattis pellentesque id nibh tortor id aliquet lectus proin. Iaculis at erat pellentesque adipiscing commodo. Hendrerit dolor magna eget est lorem ipsum. Quis risus sed vulputate odio ut. Sem et tortor consequat id. Ipsum dolor sit amet consectetur adipiscing elit.
  
//   Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate sapien. Feugiat vivamus at augue eget arcu dictum varius duis. A iaculis at erat pellentesque adipiscing commodo elit at. Ac feugiat sed lectus vestibulum mattis ullamcorper. Lorem ipsum dolor sit amet consectetur. Senectus et netus et malesuada fames ac turpis egestas integer. Morbi tristique senectus et netus et malesuada fames ac. Ornare lectus sit amet est placerat in egestas erat. Ornare aenean euismod elementum nisi quis eleifend. Enim tortor at auctor urna nunc id. Arcu cursus euismod quis viverra. In tellus integer feugiat scelerisque varius morbi enim nunc. Varius morbi enim nunc faucibus a pellentesque sit. Id ornare arcu odio ut sem. Convallis a cras semper auctor neque vitae tempus quam. Ipsum nunc aliquet bibendum enim facilisis.
//   </Content>
// )}
// />

export default withRouter(Subscription);
