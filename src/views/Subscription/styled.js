import styled from 'styled-components'

import CommonContainer from 'common/Container'

const Container = styled(CommonContainer)`
  position: relative;
  background-color: #76d7c3;
  height: 100%;
  min-height: 100vh;
  width: 100%;
`

const Button = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 30px;
  background-color: #eb984e;
  border: none;
  outline: none;
  margin-top: 60px;
  margin-left: 60px;
`;

const Content = styled.div`
  background-color: #fff;
  width: 100%;
  height: 100%;
  position: relative;
  overflow-y: scroll;
`;

export {
  Container,
  Button,
  Content
}
