import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';

import {
  Container
} from './styled';

class Stack extends Component {
  render() {
    const { goBack } = this.props.history;

    return (
      <Container>
        <button onClick={() => goBack()}>
          go back
        </button>
      </Container>
    )
  }
}

export default withRouter(Stack)
