import styled from 'styled-components'

import CommonContainer from 'common/Container'

const Container = styled(CommonContainer)`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`

export {
  Container
}
