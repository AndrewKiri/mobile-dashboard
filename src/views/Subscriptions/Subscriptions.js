import React, { Component } from 'react'
import { Link } from 'react-router-dom';

import {
  Container
} from './styled'

export default class Subscriptions extends Component {
  render () {
    return (
      <Container className='Subscriptions'>
        <Link to='/subscriptions/1?status=active&id=nintendo-switch'>
          Open stack
        </Link>
      </Container>
    )
  }
}
