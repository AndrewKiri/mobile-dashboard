import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import queryString from 'query-string';
import { CSSTransition, SwitchTransition } from "react-transition-group";

import Stack from '../Stack';
import Subscriptions from '../Subscriptions';
import "./styles.css";

const modes = {
  out: "out-in",
  in: "in-out"
};

const FadeTransition = props => (
  <CSSTransition
    {...props}
    classNames={'route'}
    addEndListener={(node, done) => {
      node.addEventListener("transitionend", done, false);
    }}
  />
);

const FadeReverseTransition = props => (
  <CSSTransition
    {...props}
    classNames={'route-reverse'}
    addEndListener={(node, done) => {
      node.addEventListener("transitionend", done, false);
    }}
  />
)

class Wrapper extends Component {
  render() {
    const query = queryString.parse(this.props.location.search);
    const hasFilters = Object.keys(query).length > 0 && Object.values(query).every(i => Boolean(i));
    console.log('hasFilters', hasFilters);

    return (
      <SwitchTransition mode={modes.out}>
          {hasFilters
            ? (
              <FadeTransition key={hasFilters}>
                <Stack />
              </FadeTransition>
            )
            : (
              <FadeReverseTransition key={hasFilters}>
                <Subscriptions />
              </FadeReverseTransition>
            )
          }
      </SwitchTransition>
    )
  }
}

export default withRouter(Wrapper);
