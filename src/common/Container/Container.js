import React from 'react'

import Element from './styled'

function Container (props, ref) {
  return (<Element {...props} ref={ref} />)
}

export default React.forwardRef(Container)
