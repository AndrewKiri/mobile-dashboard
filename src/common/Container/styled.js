import styled from 'styled-components'

const Element = styled.div`
  display: flex;
  flex-wrap: nowrap;
  box-sizing: border-box;
`

export default Element
