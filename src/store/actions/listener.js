const prefix = 'LISTENER'

export default {
  DO_SOMETHING_REQUEST: `${prefix}/DO_SOMETHING_REQUEST`,
}
