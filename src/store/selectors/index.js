import { createSelector } from 'reselect'

export const routerSelector = state => state.router
export const subscriptionsSelector = state => state.subscriptions
export const subscriptionSelector = state => state.subscription

export const locationSelector = createSelector(
  routerSelector,
  router => router.location || {}
)

export const pathnameSelector = createSelector(
  locationSelector,
  location => location.pathname
)

export const searchSelector = createSelector(
  locationSelector,
  location => location.search
)

export const subscriptionsItemsSelector = createSelector(
  subscriptionsSelector,
  subscriptions => subscriptions.items
)

export const subscriptionItemsSelector = createSelector(
  subscriptionSelector,
  subscription => subscription.items
)

// export const subscriptionItemSelector = createSelector(

// )
