import { connectRouter } from 'connected-react-router'
import { combineReducers } from 'redux'

import subscription from './subscription'
import subscriptions from './subscriptions'

export default history => combineReducers({
  router: connectRouter(history),
  subscription,
  subscriptions
})
