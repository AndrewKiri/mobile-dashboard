import produce from 'immer'

import items from '../api/mocks/subscriptions'

// import { workerActions } from '../actions';

const initialState = {
  items
}

export default (state = initialState, action) => produce(state, (draft) => {
  switch (action.type) {
    // case workerActions.SET_STATUS: {
    //   const { status, id } = action.payload;
    //   const recordIndex = draft.records.findIndex((el) => el.key === id);

    //   draft.records[recordIndex].status = status;
    //   break;
    // }

    default:
      break
  }
})
