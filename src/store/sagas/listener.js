import { takeEvery } from 'redux-saga/effects'
import { listenerActions } from '../actions'

import {
  doSomething
} from './worker'

export default function * () {
  yield takeEvery(listenerActions.DO_SOMETHING_REQUEST, doSomething)
}
