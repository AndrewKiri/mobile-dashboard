export default [
  {
    entity: 'subscription',
    title: 'Google Pixel 3a',
    status: 'processing',
    color: '#F4722A',
    id: 1,
    priceCents: 2490,
    note: {
      type: 'processing',
      data: {
        text: 'Reviewing order'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'iPhone Xs 64GB',
    status: 'active',
    color: '#F4722A',
    id: 2,
    priceCents: 2490,
    note: {
      type: 'nextPayment',
      data: {
        method: 'card',
        date: 'May 16th'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'DJI Mavic 2 Pro',
    status: 'active',
    color: '#068EF9',
    priceCents: 2490,
    id: 3,
    note: {
      type: 'nextPayment',
      data: {
        method: 'paypal',
        date: 'May 20th'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'Nest Home Hub',
    color: '#068EF9',
    status: 'active',
    priceCents: 2490,
    id: 4,
    note: {
      type: 'nextPayment',
      data: {
        method: 'paypal',
        date: 'May 22nd'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'Nest Home Hub',
    color: '#068EF9',
    status: 'active',
    priceCents: 2490,
    id: 5,
    note: {
      type: 'nextPayment',
      data: {
        method: 'paypal',
        date: 'May 20th'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'Nest Home Hub',
    color: '#068EF9',
    status: 'active',
    priceCents: 2490,
    id: 6,
    note: {
      type: 'nextPayment',
      data: {
        method: 'paypal',
        date: 'May 20th'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'Nintendo Switch',
    color: '#A6D122',
    status: 'active',
    priceCents: 2490,
    id: 7,
    note: {
      type: 'nextPayment',
      data: {
        method: 'paypal',
        date: 'May 20th'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'Nintendo Switch',
    color: '#A6D122',
    priceCents: 2490,
    status: 'active',
    id: 8,
    note: {
      type: 'nextPayment',
      data: {
        method: 'paypal',
        date: 'May 20th'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'GoPro Hero 5',
    status: 'active',
    color: '#A6D122',
    priceCents: 2490,
    id: 9,
    note: {
      type: 'nextPayment',
      data: {
        method: 'paypal',
        date: 'May 20th'
      }
    }
  },
  {
    entity: 'subscription',
    title: 'Lumix LX-3000',
    status: 'ended',
    color: '#000',
    priceCents: 2490,
    id: 10,
    note: {
      type: 'ended',
      data: {
        text: 'returned'
      }
    }
  }
]
