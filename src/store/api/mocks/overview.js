export default [
  {
    title: 'Processing',
    amount: 1,
    preview: [
      {
        entity: 'subscription',
        title: 'Google Pixel 3a',
        status: 'processing',
        id: 1,
        note: {
          type: 'transit',
          data: {
            statusString: 'Reviewing order',
            color: '#F4722A'
          }
        }
      }
    ]
  },
  {
    title: 'Active',
    amount: 6,
    filter: {
      type: 'status',
      properties: {
        status: 'active'
      }
    },
    preview: [
      {
        entity: 'stack',
        title: 'Nest Home Hub',
        amount: 3,
        color: '#B819BB',
        filter: {
          type: 'product',
          properties: {
            id: '4938',
            status: 'active'
          }
        }
      },
      {
        entity: 'subscription',
        title: 'iPhone Xs 64GB',
        status: 'active',
        id: 3,
        note: {
          type: 'nextPayment',
          data: {
            method: 'card',
            color: '#F4722A',
            date: 'May 16th'
          }
        }
      },
      {
        entity: 'subscription',
        title: 'DJI Mavic 2 Pro',
        status: 'active',
        id: 4,
        note: {
          type: 'nextPayment',
          data: {
            method: 'paypal',
            color: '#068EF9',
            date: 'May 20th'
          }
        }
      }
    ]
  },
  {
    title: 'Past',
    amount: 1,
    preview: [
      {
        entity: 'subscription',
        title: 'Lumix LX-3000',
        status: 'ended',
        id: 4,
        note: {
          type: 'returned'
        }
      }
    ]
  }
]
