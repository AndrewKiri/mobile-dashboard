import { createBrowserHistory } from 'history'
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'connected-react-router'
import { logger } from 'redux-logger'
import createRootReducer from './reducers'
import rootSaga from './sagas'

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const sagaMiddleware = createSagaMiddleware(rootSaga)

export const history = createBrowserHistory()
export const runSaga = () => sagaMiddleware.run(rootSaga)

const middleware = composeEnhancers(
  applyMiddleware(
    routerMiddleware(history),
    logger,
    sagaMiddleware
  )
)

const configureStore = (preloadedState) => createStore(
  createRootReducer(history),
  preloadedState,
  middleware
)

export default configureStore
