import React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { Route, Switch } from 'react-router-dom'

import Empty from 'views/Empty'
import Subscription from 'views/Subscription'
import Wrapper from 'views/Wrapper'

import Main from 'components/Main'
import Content from 'components/Content'
import Sidebar from 'components/Sidebar'

import configureStore, { history, runSaga } from './store'

const store = configureStore()
runSaga()

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Main>
        <Sidebar>
          <Switch>
            <Route
              path="/"
              component={Wrapper}
            />
          </Switch>
        </Sidebar>
        
        <Content>
          <Switch>
            <Route
              path="/subscriptions/:id"
              component={Subscription}
              exact
            />
            <Route
              path="/"
              component={Empty}
            />
          </Switch>
        </Content>
      </Main>
    </ConnectedRouter>
  </Provider>
)



export default App
