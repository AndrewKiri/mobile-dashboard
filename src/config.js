export const routeLevels = [0, 1, 2];

export const paths = {
  '/': 0,
  '/subscriptions/': 1,
  '/subscriptions/:id': 2,
}
